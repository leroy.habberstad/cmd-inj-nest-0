
const express = require('express')
const bodyParser = require('body-parser');

const app = express()
const port = 3000

app.set('view engine', 'ejs'); //for template injection
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'))
app.use('/nest_test', require('./vulnerabilities/nest_test'));


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
    